<?php global $wp_metapress_textdomain; ?>
<div class="metapress-admin-header metapress-border-box">
    <div class="metapress-admin-logo">
    <img src="<?php echo METAPRESS_PLUGIN_BASE_URL.'images/metapress-logo-icon.png'; ?>">
    </div>
    <div class="metapress-admin-header-text">
        <span><?php _e('How do you like the MetaPress plugin?', $wp_metapress_textdomain); ?></span><a class="button button-primary" href="https://metapress.ca/add-review/?uat=722c0bbe2f10c688031d196175b5f458088e7928ce0efc22a81afe98c5777557" target="_blank"><?php _e('Leave Review', $wp_metapress_textdomain); ?></a>
        <a class="button" href="https://metapress.ca/donate/" target="_blank"><?php _e('Donate', $wp_metapress_textdomain); ?></a>
    </div>
</div>
