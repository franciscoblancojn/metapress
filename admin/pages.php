<?php

class METAPRESS_PLUGIN_SETTINGS_MANAGER {
  public $text_domain;

  public function __construct() {
    global $wp_metapress_textdomain;
    $this->text_domain = $wp_metapress_textdomain;
    add_action( 'admin_init', array($this, 'register_metapress_settings') );
    add_action( 'admin_menu', array($this, 'add_metapress_admin_pages') );
  }

  public function register_metapress_settings() {
    register_setting('metapress-plugin-options', 'metapress_live_mode');
    register_setting('metapress-plugin-options', 'metapress_wallet_addresses', array(
        'type' => 'array',
        'default' => array(
          array(
            'name'    => __('Primary', $this->text_domain),
            'address' => ''
          )
        )
    ));
    register_setting('metapress-plugin-options', 'metapress_test_wallet_addresses', array(
        'type' => 'array',
        'default' => array(
          array(
            'name'    => __('Test Primary', $this->text_domain),
            'address' => ''
          )
        )
    ));
    register_setting('metapress-plugin-options', 'metapress_allowed_test_address');
    register_setting('metapress-plugin-options', 'metapress_binance_cron');
    register_setting('metapress-plugin-options', 'metapress_access_tokens_expire');
    register_setting('metapress-plugin-options', 'metapress_supported_post_types', array(
        'type' => 'array',
        'default' => array('post', 'page')
    ));
    register_setting('metapress-plugin-options', 'metapress_woocommerce_filters_enabled');
    register_setting('metapress-plugin-options', 'metapress_checkout_page');
    register_setting('metapress-plugin-options', 'metapress_transactions_page');
    register_setting('metapress-plugin-tokens', 'metapress_custom_tokens_list');
    register_setting('metapress-plugin-nft-contracts', 'metapress_nft_contract_list');
    register_setting('metapress-plugin-nft-contracts', 'metapress_opensea_api_key');
    register_setting('metapress-plugin-language', 'metapress_text_settings');

    register_setting('metapress-plugin-networks', 'metapress_supported_networks', array(
        'type' => 'array',
        'default' => array(
            array(
                'name' => __('Ethereum Mainnet', $this->text_domain),
                'slug' => 'mainnet',
                'chainid' => '0x1',
                'symbol' => 'ETH',
                'receiving_address' => '',
                'explorer' => 'https://etherscan.io/',
                'icon' => METAPRESS_PLUGIN_BASE_URL.'images/ethereum.png',
                'enabled' => 1
            ),
            array(
                'name' => __('Polygon (MATIC)', $this->text_domain),
                'slug' => 'maticmainnet',
                'chainid' => '0x89',
                'symbol' => 'MATIC',
                'receiving_address' => '',
                'explorer' => 'https://polygonscan.com/',
                'icon' => METAPRESS_PLUGIN_BASE_URL.'images/polygon.png',
                'enabled' => 1
            ),
            array(
                'name' => __('Binance Smart Chain', $this->text_domain),
                'slug' => 'binancesmartchain',
                'chainid' => '0x38',
                'symbol' => 'BNB',
                'receiving_address' => '',
                'explorer' => 'https://bscscan.com/',
                'icon' => METAPRESS_PLUGIN_BASE_URL.'images/bnb.png',
                'enabled' => 0
            )
        )
    ));

    register_setting('metapress-plugin-networks', 'metapress_supported_test_networks', array(
        'type' => 'array',
        'default' => array(
            array(
                'name' => __('Ethereum Ropsten', $this->text_domain),
                'slug' => 'ropsten',
                'chainid' => '0x3',
                'symbol' => 'ETH',
                'receiving_address' => '',
                'explorer' => 'https://ropsten.etherscan.io/',
                'enabled' => 1
            ),
            array(
                'name' => __('Polygon Mumbai', $this->text_domain),
                'slug' => 'matictestnet',
                'chainid' => '0x80001',
                'symbol' => 'MATIC',
                'receiving_address' => '',
                'explorer' => 'https://mumbai.polygonscan.com/',
                'enabled' => 1
            ),
            array(
                'name' => __('Binance Smart Chain', $this->text_domain),
                'slug' => 'binancetestnet',
                'chainid' => '0x61',
                'symbol' => 'BNB',
                'receiving_address' => '',
                'explorer' => 'https://testnet.bscscan.com/',
                'enabled' => 0
            )
        )
    ));

  }

  public function add_metapress_admin_pages() {
      add_menu_page(
          __( 'MetaPress', $this->text_domain),
          'MetaPress',
          'manage_options',
          'metapress-settings',
          array($this, 'load_admin_settings_page'),
          'dashicons-database',
          30
      );
      add_submenu_page( 'metapress-settings', __( 'MetaPress Settings', $this->text_domain), 'Settings', 'manage_options', 'metapress-settings', array($this, 'load_admin_settings_page') );
      add_submenu_page( 'metapress-settings', 'Payments', 'Payments', 'manage_options', 'metapress-payments', array($this, 'load_admin_payments_page') );
      add_submenu_page( 'metapress-settings', 'Payment Tokens', 'Payment Tokens', 'manage_options', 'metapress-tokens', array($this, 'load_admin_tokens_page') );
      add_submenu_page( 'metapress-settings', 'Smart Contracts (NFTs)', 'Smart Contracts (NFTs)', 'manage_options', 'metapress-nft-contracts', array($this, 'load_admin_nft_contracts_page') );
      add_submenu_page( 'metapress-settings', 'Networks', 'Networks', 'manage_options', 'metapress-networks', array($this, 'load_admin_networks_page') );
      add_submenu_page( 'metapress-settings', __( 'Language', $this->text_domain), 'Language', 'manage_options', 'metapress-language-settings', array($this, 'load_admin_language_settings_page') );
  }

  public function load_admin_settings_page() {
      require_once('admin-settings.php');
  }
  public function load_admin_payments_page() {
      wp_enqueue_style('jquery-ui');
      wp_enqueue_style('metapress-datepicker-admin');
      wp_enqueue_script('jquery-ui-datepicker');
      wp_enqueue_script( 'metapress-admin-payments' );
      require_once('admin-payments.php');
  }
  public function load_admin_tokens_page() {
      $metapress_supported_networks = get_option('metapress_supported_networks');
      $metapress_supported_test_networks = get_option('metapress_supported_test_networks');
      wp_enqueue_media();
      wp_enqueue_style('jquery-ui');
      wp_enqueue_script( 'metapress-admin-image-upload' );
      wp_enqueue_script( 'metapress-admin-tokens' );
      wp_localize_script('metapress-admin-tokens', 'metapressjsdata', array(
          'network_options' => $metapress_supported_networks,
          'test_network_options' => $metapress_supported_test_networks,
      ));
      require_once('admin-tokens.php');
  }
  public function load_admin_nft_contracts_page() {
      $metapress_supported_networks = get_option('metapress_supported_networks');
      wp_enqueue_media();
      wp_enqueue_style('jquery-ui');
      wp_enqueue_script( 'metapress-admin-nfts' );
      wp_localize_script('metapress-admin-nfts', 'metapressjsdata', array(
          'network_options' => $metapress_supported_networks,
      ));
      require_once('admin-nft-contracts.php');
  }

  public function load_admin_networks_page() {
      // $metapress_live_mode = get_option('metapress_live_mode', 0);
      // wp_enqueue_media();
      // wp_enqueue_script( 'metapress-admin-image-upload' );
      // wp_enqueue_script( 'metapress-admin-networks' );
      // wp_localize_script('metapress-admin-networks', 'metapressjsdata', array(
      //     'live_mode' => $metapress_live_mode,
      // ));
      require_once('admin-networks.php');
  }

  public function load_admin_language_settings_page() {
      require_once('admin-language-settings.php');
  }
}
$metapress_plugin_settings_manager = new METAPRESS_PLUGIN_SETTINGS_MANAGER();
