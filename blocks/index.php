<?php

class METAPRESS_GUTENBERG_BLOCKS_MANAGER {

	protected $assets_path;

	private $supported_networks;
    private $custom_tokens = array();
	protected $live_mode;

	public function __construct() {
		if ( ! function_exists( 'register_block_type' ) ) {
			return;
		}
		$this->assets_path = plugin_dir_url(__FILE__);
		$metapress_live_mode = get_option('metapress_live_mode', 0);
        if( $metapress_live_mode ) {
			$this->live_mode = true;
			$this->supported_networks = get_option('metapress_supported_networks');
        } else {
			$this->live_mode = false;
			$this->supported_networks = get_option('metapress_supported_test_networks');
        }
		$this->custom_tokens = get_option('metapress_custom_tokens_list', array());
		add_action( 'init', array($this, 'create_metapress_restricted_content_block') );
		add_filter( 'render_block', array($this, 'metapress_block_filter'), 10, 2 );
	}

	public function create_metapress_restricted_content_block() {
		global $wp_metapress_version;

		wp_register_script('metapress-restricted-content-block-js', $this->assets_path .'/block.js', array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' ), $wp_metapress_version);

	  wp_localize_script( 'metapress-restricted-content-block-js', 'metapressblockcontent',
	      array(
					'product_list' => $this->create_editor_product_list(),
				)
		);

		wp_register_style(
			'metapress-gutenberg-block-editor-css',
			$this->assets_path .'/editor.css',
			array( 'wp-edit-blocks' ),
			$wp_metapress_version
		);

		wp_register_style(
			'metapress-restricted-content-block-css',
			$this->assets_path .'/style.css',
			array(),
			$wp_metapress_version
		);

		register_block_type( 'metapress-blocks/restricted-content-block', array(
			'style' => 'metapress-restricted-content-block-css',
			'editor_style' => 'metapress-gutenberg-block-editor-css',
			'editor_script' => 'metapress-restricted-content-block-js',
	        'attributes'      => array(
	            'product_id'    => array(
	                'type'      => 'number',
	                'default'   => 0,
	            ),
	        ),
		) );
	}

	public function metapress_block_filter( $block_content, $block ) {
		global $wp_metapress_textdomain;
		global $wp_metapress_text_settings;
		$metapress_restricted_content = $block_content;
		if ( $block['blockName'] === 'metapress-blocks/restricted-content-block' ) {
				$product_id = null;
				if( isset($block['attrs']['product_id']) && ! empty($block['attrs']['product_id']) ) {
						$product_id = $block['attrs']['product_id'];
				}

				if( ! empty($product_id) ) {
					$access_token = null;
					$user_has_access = false; // get user payments (check for access)
					if( isset($_GET['mpatok']) && ! empty($_GET['mpatok']) ) {
						$metapress_access_token = sanitize_key($_GET['mpatok']);
						$metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
	          			$access_token = $metapress_access_token_manager->access_token($product_id, $metapress_access_token);
						if( ! empty($access_token) ) {
							if( isset($access_token->expires) && ($access_token->expires > current_time('timestamp') ) ) {
									$user_has_access = true;
							} else {
									$metapress_access_token_manager->delete_access_token($access_token->id);
							}
						}
					}
					$data = get_option("metapress_contract_addresses");
					$network_button_receiving_address = $data->matictestnet;
					if( ! $user_has_access ) {
						$product_price = get_post_meta( $product_id, 'product_price', true );
						$metapress_restricted_content = '<div class="metapress-restricted-access" data-product-id="'.$product_id.'">';
						$metapress_restricted_content .= '<p class="metapress-restricted-text">'.$wp_metapress_text_settings['restricted_text'].'</p>';

						$metapress_restricted_content .= '<div class="metapress-login-notice"><p><small>*'.__('Accessing this content requires a', $wp_metapress_textdomain).' <a href="https://metamask.io/" target="_blank">MetaMask</a> '.__('account', $wp_metapress_textdomain).'</small>.</p><button class="metamask-connect-wallet">'.__('Connect Wallet', $wp_metapress_textdomain).'</button></div>';

						$metapress_restricted_content .= '<div class="metapress-access-buttons">';

						if( ! empty($product_price) && $product_price > 0 ) {
							if( ! empty($this->supported_networks) ) {
								foreach($this->supported_networks as $network_button) {
									if( isset($network_button['enabled']) ) {
										$network_pay_with_text = esc_attr(__('Pay with ', $wp_metapress_textdomain).$network_button['symbol']);
										$metapress_restricted_content .= '<div class="metapress-payment-button" data-product-id="'.$product_id.'" data-token="'.esc_attr($network_button['symbol']).'" data-network="'.esc_attr($network_button['slug']).'" data-networkname="'.esc_attr($network_button['name']).'" data-chainid="'.esc_attr($network_button['chainid']).'"';
										$metapress_restricted_content .= 'data-wallet="'.esc_attr($network_button['receiving_address']).'" data-explorer="'.esc_attr($network_button['explorer']).'"><img src="'.esc_attr($network_button['icon']).'" alt="'.$network_pay_with_text.'" /> <span class="metapress-payment-button-text">'.$network_pay_with_text.'</span><span class="metapress-payment-button-amount"></span></div> ';
									}
								}
							}

							// ADD CUSTOM TOKENS
		                    if( ! empty($this->custom_tokens) ) {
		                        foreach($this->custom_tokens as $custom_token) {
									if( isset($custom_token['enabled']) ) {
			                            $pay_with_text = __('Pay with ', $wp_metapress_textdomain).$custom_token['name'];
										if( $this->live_mode ) {
	                                        $token_network = esc_attr($custom_token['network']);
	                                        $token_network_name = esc_attr($custom_token['networkname']);
	                                        $token_network_chainid = esc_attr($custom_token['chainid']);
											$token_network_explorer = esc_attr($custom_token['explorer']);
	                                        $add_test_token_class = "";
	                                    } else {
	                                        $token_network = esc_attr($custom_token['test_network']);
	                                        $token_network_name = esc_attr($custom_token['test_networkname']);
	                                        $token_network_chainid = esc_attr($custom_token['test_chainid']);
											$token_network_explorer = esc_attr($custom_token['test_explorer']);
	                                        $add_test_token_class = " test-token";
	                                    }
										$metapress_restricted_content .= '<div class="metapress-payment-button'.$add_test_token_class.'" data-product-id="'.$product_id.'" data-token="'.$custom_token['currency_symbol'].'" data-network="'.$token_network.'" data-address="'.$custom_token['contract_address'].'"  data-test-address="'.$custom_token['test_contract_address'].'"';

	                                    $metapress_restricted_content .= 'data-explorer="'.$token_network_explorer.'" data-networkname="'.$token_network_name.'" data-chainid="'.$token_network_chainid.'"><img src="'.$custom_token['icon'].'" alt="'.$pay_with_text.'" /> <span class="metapress-payment-button-text">'.$pay_with_text.'</span><span class="metapress-payment-button-amount"></span></div> ';
									}
		                        }
		                    }
							// END CUSTOM TOKENS
						}

						// CHECK FOR NFT ACCESS
						$product_nft_access_list = get_post_meta($product_id, 'product_nft_access_list', true );

						if( ! empty($product_nft_access_list) ) {
							$nft_access_nonce = wp_create_nonce('metapress-nft-verified-nonce');
							$metapress_restricted_content .= '<div id="metapress-nft-verification-text" data-nonce="'.$nft_access_nonce.'"><p>'.$wp_metapress_text_settings['ownership_verification_text'].'.</p></div>';
				            foreach($product_nft_access_list as $nft_token) {
								$nft_collection_slug = "";
								if( isset($nft_token['token_image']) && ! empty($nft_token['token_image']) ) {
									$nft_asset_image = $nft_token['token_image'];
								} else {
									$nft_asset_image = METAPRESS_PLUGIN_BASE_URL.'images/metapress-logo-icon.png';
								}

								if( isset($nft_token['collection_slug']) && ! empty($nft_token['collection_slug']) ) {
									$nft_collection_slug = $nft_token['collection_slug'];
								}

								$metapress_restricted_content .= '<div class="metapress-verify-nft-owner" data-token="'.$nft_token['token_id'].'" data-contract-address="'.$nft_token['contract_address'].'" data-token-type="'.$nft_token['token_type'].'" data-network="'.$nft_token['network'].'" data-networkname="'.$nft_token['networkname'].'" data-chainid="'.$nft_token['chainid'].'" data-product-id="'.$product_id.'" data-collection="'.$nft_collection_slug.'">';
								$metapress_restricted_content .= '<img src="'.$nft_asset_image.'" alt="'.$nft_token['token_name'].'" /> <span class="nft-name">'.$nft_token['token_name'].'</span>';
								$metapress_restricted_content .= '<div class="metapress-verify-buttons"><div class="metapress-verify-button"><span class="verify-text">'.__('Confirm Ownership', $wp_metapress_textdomain).'</span></div><div class="metapress-verify-link"><a class="nft-link" href="'.$nft_token['token_url'].'" target="_blank" title="'.__('View NFT', $wp_metapress_textdomain).'">'.__('View', $wp_metapress_textdomain).' <span class="dashicons dashicons-external"></span></a></div></div></div>';
							}
						}

						$metapress_restricted_content .= '</div><div class="metapress-notice-box"></div></div>';
					} else {
						if( ! empty($access_token) ) {
								$metapress_restricted_content .= '<div class="metapress-verification" data-address="'.$access_token->payment_owner.'" data-token="'.$access_token->token.'"></div>';
						}
					}
				}
    }
		return $metapress_restricted_content;
	}

	protected function create_editor_product_list() {
			$product_list = array(array(
				'product_id' => 0,
				'product_name' => 'None'
			));
			$metapress_product_args = array(
					'post_type' => 'metapress_product',
					'posts_per_page' => -1,
					'post_status' => 'publish'
			);
			$metapress_products = get_posts($metapress_product_args);
			if( ! empty($metapress_products) ) {
					foreach($metapress_products as $product) {
							$product_list[] = array(
									'product_id' => $product->ID,
									'product_name' => $product->post_title
							);
					}
			}
			return $product_list;
	}
}
$metapress_gutenberg_blocks_manager = new METAPRESS_GUTENBERG_BLOCKS_MANAGER();
