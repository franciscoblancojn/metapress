<?php
require_once('custom-meta-functions.php');
require_once('product/config.php');

class METAPRESS_PLUGIN_RUN_SETUP_MANAGER {

    public function __construct() {
        $this->run_setup();
    }

    public function run_setup() {
        $metapress_contract_addresses = get_option('metapress_contract_addresses');
        $metapress_api_request_match = get_option('metapress_api_request_match');
        $metapress_supported_networks = get_option('metapress_supported_networks');
        $metapress_supported_test_networks = get_option('metapress_supported_test_networks');
        if( empty($metapress_contract_addresses) ) {
          $set_metpress_contact_address = (object) array(
              'ropsten' => '0x9A434d8c8B9C99ACa036130055511Dc5152a5ae2',
              'mainnet' => '0x00c5A679d3Ae7261e021FF80DF210De513b38042',
              'matictestnet' => '0xaddBF54A1E826436257f05E785881133f9895141',
              'maticmainnet' => '0x2486bD244919E55d8b98053c46e4eB6c10ACf9E7',
              'binancetestnet' => '0x9bC293b22c74a8b2eEd677e77A7c90c7aE34ace4',
              'binancesmartchain' => '0x9bC293b22c74a8b2eEd677e77A7c90c7aE34ace4'
          );
          update_option('metapress_contract_addresses', $set_metpress_contact_address);
        }

        if( empty($metapress_api_request_match) ) {
            $metapress_api_request_match = $this->generate_new_api_key(26);
            update_option('metapress_api_request_match', $metapress_api_request_match);
        }

        if( empty($metapress_supported_networks) ) {
            $metapress_supported_networks = array(
                array(
                    'name' => 'Ethereum Mainnet',
                    'slug' => 'mainnet',
                    'chainid' => '0x1',
                    'symbol' => 'ETH',
                    'receiving_address' => '',
                    'explorer' => 'https://etherscan.io/',
                    'icon' => METAPRESS_PLUGIN_BASE_URL.'images/ethereum.png',
                    'enabled' => 1
                ),
                array(
                    'name' => 'Polygon (MATIC)',
                    'slug' => 'maticmainnet',
                    'chainid' => '0x89',
                    'symbol' => 'MATIC',
                    'receiving_address' => '',
                    'explorer' => 'https://polygonscan.com/',
                    'icon' => METAPRESS_PLUGIN_BASE_URL.'images/polygon.png',
                    'enabled' => 1
                ),
                array(
                    'name' => 'Binance Smart Chain',
                    'slug' => 'binancesmartchain',
                    'chainid' => '0x38',
                    'symbol' => 'BNB',
                    'receiving_address' => '',
                    'explorer' => 'https://bscscan.com/',
                    'icon' => METAPRESS_PLUGIN_BASE_URL.'images/bnb.png',
                    'enabled' => 1
                )
            );
            update_option('metapress_supported_networks', $metapress_supported_networks);
        }

        if( empty($metapress_supported_test_networks) ) {
            $metapress_supported_test_networks = array(
                array(
                    'name' => 'Ethereum Ropsten',
                    'slug' => 'ropsten',
                    'chainid' => '0x3',
                    'symbol' => 'ETH',
                    'receiving_address' => '',
                    'explorer' => 'https://ropsten.etherscan.io/',
                    'icon' => METAPRESS_PLUGIN_BASE_URL.'images/ethereum.png',
                    'enabled' => 1
                ),
                array(
                    'name' => 'Polygon Mumbai',
                    'slug' => 'matictestnet',
                    'chainid' => '0x80001',
                    'symbol' => 'MATIC',
                    'receiving_address' => '',
                    'explorer' => 'https://mumbai.polygonscan.com/',
                    'icon' => METAPRESS_PLUGIN_BASE_URL.'images/polygon.png',
                    'enabled' => 1
                ),
                array(
                    'name' => 'Binance Smart Chain Testnet',
                    'slug' => 'binancetestnet',
                    'chainid' => '0x61',
                    'symbol' => 'BNB',
                    'receiving_address' => '',
                    'explorer' => 'https://testnet.bscscan.com/',
                    'icon' => METAPRESS_PLUGIN_BASE_URL.'images/bnb.png',
                    'enabled' => 1
                )
            );
            update_option('metapress_supported_test_networks', $metapress_supported_test_networks);
        }


    }

    private function generate_new_api_key($length) {
        if( function_exists('random_bytes') ) {
            return bin2hex(random_bytes(intval($length)));
        } else {
            $cry_strong = true;
            $random_bytes = openssl_random_pseudo_bytes(intval($length), $cry_strong);
            return bin2hex($random_bytes);
        }
    }
}
$metapress_plugin_run_setup_manager = new METAPRESS_PLUGIN_RUN_SETUP_MANAGER();
