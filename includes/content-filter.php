<?php

class MetaPress_Plugin_Content_Filter_Shortcodes {

    private $supported_networks;
    private $custom_tokens = array();
    protected $live_mode;

    public function __construct() {
        $metapress_live_mode = get_option('metapress_live_mode', 0);
        if( $metapress_live_mode ) {
            $this->live_mode = true;
            $this->supported_networks = get_option('metapress_supported_networks');
        } else {
            $this->live_mode = false;
            $this->supported_networks = get_option('metapress_supported_test_networks');
        }
        $this->custom_tokens = get_option('metapress_custom_tokens_list', array());
        add_filter('the_content', array($this,'metapress_restrict_post_type_content_with_products'));
        add_filter('the_content', array($this,'metapress_filter_metaproduct_content'));
		add_shortcode('metapress-checkout', array($this, 'metapress_product_checkout_shortcode'));
        add_shortcode('metapress-restricted-content', array($this, 'metapress_restricted_content_shortcode'));
		add_shortcode('metapress-transactions', array($this, 'metapress_user_transactions_shortcode'));
    }

    public function metapress_restrict_post_type_content_with_products( $content ) {
		global $wp_metapress_textdomain;
        global $wp_metapress_text_settings;
        global $post;
        $metapress_restricted_content = $content;
        $metapress_required_products = get_post_meta( $post->ID, 'metapress_required_products', true );
        $metapress_woocommerce_filters_enabled = get_option('metapress_woocommerce_filters_enabled');
        if( $metapress_woocommerce_filters_enabled && $post->post_type == 'product' ) {
            return $metapress_restricted_content;
        }
        if ( ! empty($metapress_required_products) ) {
            $user_has_access = false; // get user payments (check for access)
            $metapress_products_list = implode(',', $metapress_required_products);
            foreach($metapress_required_products as $product_id) {
                $metapress_product = get_post($product_id);

                if( ! empty($metapress_product) ) {
                    $access_token = null;
                    $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
                    if( isset($_GET['mpatok']) && ! empty($_GET['mpatok']) ) {
                        $metapress_access_token = sanitize_key($_GET['mpatok']);
                        $access_token = $metapress_access_token_manager->access_token($product_id, $metapress_access_token);
                        if( ! empty($access_token) ) {
                            if( isset($access_token->expires) && ($access_token->expires > current_time('timestamp') ) ) {
                                $user_has_access = true;
                                break;
                            } else {
                                $metapress_access_token_manager->delete_access_token($access_token->id);
                            }
                        }
                    }
                }
            }

            if( ! $user_has_access ) {
                $current_page_url = get_the_permalink($post->ID);
                $metapress_checkout_page = get_option('metapress_checkout_page');
                $metapress_checkout_page_url = null;
                if( ! empty($metapress_checkout_page) ) {
                    $metapress_checkout_page_url = get_the_permalink($metapress_checkout_page);
                }
                $metapress_restricted_content = '<div id="metapress-single-restricted-content" class="metapress-restricted-access has-multiple" data-product-ids="'.$metapress_products_list.'">';
                $metapress_restricted_content .= '<p class="metapress-restricted-text">'.$wp_metapress_text_settings['restricted_text'].'</p>';

                $metapress_restricted_content .= '<div class="metapress-login-notice"><p><small>*'.__('Accessing this content requires a', $wp_metapress_textdomain).' <a href="https://metamask.io/" target="_blank">MetaMask</a> '.__('account', $wp_metapress_textdomain).'</small>.</p><button class="metamask-connect-wallet">'.__('Connect Wallet', $wp_metapress_textdomain).'</button></div>';

                $metapress_restricted_content .= '<div class="metapress-access-buttons">';
                $metapress_restricted_content .= '<p class="metapress-checkout-purchase-text">'.$wp_metapress_text_settings['product_purchase_text'].':</p>';
                foreach($metapress_required_products as $product_id) {
                    $product_name = get_the_title($product_id);
                    if( ! empty($metapress_checkout_page_url) ) {
                        $product_checkout_page_url = $metapress_checkout_page_url.'?mpp='.$product_id.'&mpred='.$current_page_url;
                        $metapress_restricted_content .= '<a href="'.$product_checkout_page_url.'" class="metapress-checkout-button">'.$product_name.'</a>';
                    }
                }

                $metapress_restricted_content .= '</div><div class="metapress-notice-box"></div></div>';
            } else {
                if( ! empty($access_token) ) {
                	$metapress_restricted_content .= '<div class="metapress-verification" data-address="'.$access_token->payment_owner.'" data-token="'.$access_token->token.'"></div>';
                }
            }
        }
        return $metapress_restricted_content;
	}

    public function metapress_filter_metaproduct_content( $content ) {
        global $wp_metapress_textdomain;
        global $post;
        $metapress_restricted_content = $content;
        if ( $post && $post->post_type == 'metapress_product' ) {
            $metapress_checkout_page = get_option('metapress_checkout_page');
            $product_image = get_the_post_thumbnail_url($post->ID, 'medium');
            if( has_excerpt($post->ID) ) {
                $product_description = get_the_excerpt($post->ID);
            } else {
                $product_description = "";
            }
            $metapress_restricted_content .= '<div class="single-metapress-product metapress-border-box metapress-align-content-center">';
            $metapress_restricted_content .= '<h3>'.$post->post_title.'</h3>';
            if( ! empty($product_image) ) {
                $metapress_restricted_content .= '<div class="metapress-product-image"><img src="'.$product_image.'" alt="'.$post->post_title.'"></div>';
            }
            if( ! empty($product_description) ) {
                $metapress_restricted_content .= '<div class="metapress-product-description">'.$product_description.'</div>';
            }
            if( ! empty($metapress_checkout_page) ) {
                $metapress_checkout_page_url = get_the_permalink($metapress_checkout_page);
                $current_product_url = get_the_permalink($post->ID);
                if( ! empty($metapress_checkout_page_url) ) {
                    $metapress_checkout_page_url .= '?mpp='.$post->ID;
                    $metapress_restricted_content .= '<a href="'.$metapress_checkout_page_url.'&mpred='.$current_product_url.'" class="metapress-checkout-button">'.__('Buy Now', $wp_metapress_textdomain).'</a>';
                }
            }
            $metapress_restricted_content .= '</div>';
        }
        return $metapress_restricted_content;
	}

	public function metapress_product_checkout_shortcode( $atts ) {
		global $wp_metapress_textdomain;
        global $wp_metapress_text_settings;
		$product_id = null;
		$user_has_access = false;
		  if( isset($_GET['mpp']) && ! empty($_GET['mpp']) ) {
		      $product_id = intval($_GET['mpp']);
		  }

          if( isset($_GET['mpred']) && ! empty($_GET['mpred']) ) {
		      $metapress_redirect_url = esc_url($_GET['mpred']);
		  } else {
              $metapress_redirect_url = "";
          }

		  $metapress_restricted_content = '<p>'.__('Missing product ID', $wp_metapress_textdomain).'.</p>';
		  if( ! empty($product_id) ) {
		      $access_token = null;
		      $product_name = get_the_title($product_id);
              $product_image = get_the_post_thumbnail_url($product_id, 'medium');
              $product_description = get_the_excerpt($product_id);
              if( isset($wp_metapress_text_settings['checkout_purchasing_text']) && ! empty($wp_metapress_text_settings['checkout_purchasing_text']) ) {
                  $checkout_description = $wp_metapress_text_settings['checkout_purchasing_text'];
              } else {
                  $checkout_description = __('You are purchasing ', $wp_metapress_textdomain).'{product_title}';
              }
              $checkout_description = str_replace('{product_title}', '<strong>'.$product_name.'</strong>', $checkout_description);

		      if( isset($_GET['mpatok']) && ! empty($_GET['mpatok']) ) {
		          $metapress_access_token = sanitize_key($_GET['mpatok']);
		          $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
		          $access_token = $metapress_access_token_manager->access_token($product_id, $metapress_access_token);
		          if( ! empty($access_token) ) {
		              if( isset($access_token->expires) && ($access_token->expires > current_time('timestamp') ) ) {
		                  $user_has_access = true;
		              } else {
		                  $metapress_access_token_manager->delete_access_token($access_token->id);
		              }
		          }
		      }
		      if( ! $user_has_access ) {
                  $product_price = get_post_meta( $product_id, 'product_price', true );
		          $metapress_restricted_content = '<div class="metapress-checkout-access" data-product-id="'.$product_id.'">';
                  if( ! empty($product_image) ) {
                      $metapress_restricted_content .= '<img class="metapress-checkout-image" src="'.$product_image.'" alt="'.$product_name.'">';
                  }
    		      $metapress_restricted_content .= '<p>'.$checkout_description.'</p>';
                  if( ! empty($product_description) ) {
                      $metapress_restricted_content .= '<p class="metapress-product-description">'.$product_description.'</p>';
                  }

		          $metapress_restricted_content .= '<div class="metapress-login-notice"><p><small>*'.__('Accessing this content requires a', $wp_metapress_textdomain).' <a href="https://metamask.io/" target="_blank">MetaMask</a> '.__('account', $wp_metapress_textdomain).'</small>.</p><button class="metamask-connect-wallet">'.__('Connect Wallet', $wp_metapress_textdomain).'</button></div>';

		          $metapress_restricted_content .= '<div class="metapress-access-buttons">';

                  $data = get_option("metapress_contract_addresses");
                  $network_button_receiving_address = $data->matictestnet;
                  if( ! empty($product_price) && $product_price > 0 ) {
                      if( ! empty($this->supported_networks) ) {
                          foreach($this->supported_networks as $network_button) {
                              if( isset($network_button['enabled']) ) {
                                  $network_pay_with_text = esc_attr(__('Pay with ', $wp_metapress_textdomain).$network_button['symbol']);
                                  $metapress_restricted_content .= '<div class="metapress-payment-button" data-product-id="'.$product_id.'" data-token="'.esc_attr($network_button['symbol']).'" data-network="'.esc_attr($network_button['slug']).'" data-networkname="'.esc_attr($network_button['name']).'" data-chainid="'.esc_attr($network_button['chainid']).'"';
                                  $metapress_restricted_content .= 'data-wallet="'.esc_attr($network_button['receiving_address']).'" data-explorer="'.esc_attr($network_button['explorer']).'"><img src="'.esc_attr($network_button['icon']).'" alt="'.$network_pay_with_text.'" /> <span class="metapress-payment-button-text">'.$network_pay_with_text.'</span><span class="metapress-payment-button-amount"></span></div> ';
                              }
                          }
                      }

                      // ADD CUSTOM TOKENS
                      if( ! empty($this->custom_tokens) ) {
                          foreach($this->custom_tokens as $custom_token) {
                              if( isset($custom_token['enabled']) ) {
                                  $pay_with_text = esc_attr(__('Pay with ', $wp_metapress_textdomain).$custom_token['name']);
                                  if( $this->live_mode ) {
                                      $token_network = esc_attr($custom_token['network']);
                                      $token_network_name = esc_attr($custom_token['networkname']);
                                      $token_network_chainid = esc_attr($custom_token['chainid']);
                                      $token_network_explorer = esc_attr($custom_token['explorer']);
                                      $add_test_token_class = "";
                                  } else {
                                      $token_network = esc_attr($custom_token['test_network']);
                                      $token_network_name = esc_attr($custom_token['test_networkname']);
                                      $token_network_chainid = esc_attr($custom_token['test_chainid']);
                                      $token_network_explorer = esc_attr($custom_token['test_explorer']);
                                      $add_test_token_class = " test-token";
                                  }
                                  $metapress_restricted_content .= '<div class="metapress-payment-button'.$add_test_token_class.'" data-product-id="'.$product_id.'" data-token="'.$custom_token['currency_symbol'].'" data-network="'.$token_network.'" data-address="'.$custom_token['contract_address'].'"  data-test-address="'.$custom_token['test_contract_address'].'"';

                                  $metapress_restricted_content .= 'data-explorer="'.$token_network_explorer.'" data-networkname="'.$token_network_name.'" data-chainid="'.$token_network_chainid.'"><img src="'.$custom_token['icon'].'" alt="'.$pay_with_text.'" /> <span class="metapress-payment-button-text">'.$pay_with_text.'</span><span class="metapress-payment-button-amount"></span></div> ';
                              }
                          }
                      }
                      // END CUSTOM TOKENS
                  }

                  // CHECK FOR NFT ACCESS
                  $product_nft_access_list = get_post_meta($product_id, 'product_nft_access_list', true );

                  if( ! empty($product_nft_access_list) ) {
                      $nft_access_nonce = wp_create_nonce('metapress-nft-verified-nonce');
                      $metapress_restricted_content .= '<div id="metapress-nft-verification-text" data-nonce="'.$nft_access_nonce.'" data-redirect="'.$metapress_redirect_url.'"><p>'.$wp_metapress_text_settings['checkout_product_access_text'].'</p></div>';
                      foreach($product_nft_access_list as $nft_token) {
                          $nft_collection_slug = "";
                          if( isset($nft_token['token_image']) && ! empty($nft_token['token_image']) ) {
                              $nft_asset_image = $nft_token['token_image'];
                          } else {
                              $nft_asset_image = METAPRESS_PLUGIN_BASE_URL.'images/metapress-logo-icon.png';
                          }
                          if( isset($nft_token['collection_slug']) && ! empty($nft_token['collection_slug']) ) {
                              $nft_collection_slug = $nft_token['collection_slug'];
                          }

                          $metapress_restricted_content .= '<div class="metapress-verify-nft-owner" data-token="'.$nft_token['token_id'].'" data-contract-address="'.$nft_token['contract_address'].'" data-token-type="'.$nft_token['token_type'].'" data-network="'.$nft_token['network'].'" data-networkname="'.$nft_token['networkname'].'" data-chainid="'.$nft_token['chainid'].'" data-product-id="'.$product_id.'" data-collection="'.$nft_collection_slug.'">';
                          $metapress_restricted_content .= '<img src="'.$nft_asset_image.'" alt="'.$nft_token['token_name'].'" /> <span class="nft-name">'.$nft_token['token_name'].'</span>';
                          $metapress_restricted_content .= '<div class="metapress-verify-buttons"><div class="metapress-verify-button"><span class="verify-text">'.__('Confirm Ownership', $wp_metapress_textdomain).'</span></div><div class="metapress-verify-link"><a class="nft-link" href="'.$nft_token['token_url'].'" target="_blank" title="'.__('View NFT', $wp_metapress_textdomain).'">'.__('View', $wp_metapress_textdomain).' <span class="dashicons dashicons-external"></span></a></div></div></div>';
                      }

                  }

		          $metapress_restricted_content .= '</div><div class="metapress-notice-box"></div></div>';
		      } else {
		          $metapress_restricted_content = '<p>'.__('You already have access to this product', $wp_metapress_textdomain).'.</p>';
		      }
		  }
		  return $metapress_restricted_content;
		}

        function metapress_restricted_content_shortcode( $atts, $content ) {
            global $post;
            global $wp_metapress_text_settings;
            global $wp_metapress_textdomain;
            $metapress_restricted_content = "";
            $metapress_required_products = array();
            $metapress_shortcode_atts = shortcode_atts( array(
                'products'    => array(),
            ), $atts );

            if( isset($metapress_shortcode_atts['products']) && ! empty($metapress_shortcode_atts['products']) ) {
                $metapress_required_products = explode(",", $metapress_shortcode_atts['products']);
            }

            if ( ! empty($metapress_required_products) ) {
                $user_has_access = false; // get user payments (check for access)
                $metapress_products_list = implode(',', $metapress_required_products);
                foreach($metapress_required_products as $product_id) {
                    $metapress_product = get_post($product_id);

                    if( ! empty($metapress_product) ) {
                        $access_token = null;
                        $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
                        if( isset($_GET['mpatok']) && ! empty($_GET['mpatok']) ) {
                            $metapress_access_token = sanitize_key($_GET['mpatok']);
                            $access_token = $metapress_access_token_manager->access_token($product_id, $metapress_access_token);
                            if( ! empty($access_token) ) {
                                if( isset($access_token->expires) && ($access_token->expires > current_time('timestamp') ) ) {
                                    $user_has_access = true;
                                    break;
                                } else {
                                    $metapress_access_token_manager->delete_access_token($access_token->id);
                                }
                            }
                        }
                    }
                }

                if( ! $user_has_access ) {
                    $current_page_url = get_the_permalink($post->ID);
                    $metapress_checkout_page = get_option('metapress_checkout_page');
                    $metapress_checkout_page_url = null;
                    if( ! empty($metapress_checkout_page) ) {
                        $metapress_checkout_page_url = get_the_permalink($metapress_checkout_page);
                    }
                    $metapress_restricted_content = '<div id="metapress-single-restricted-content" class="metapress-restricted-access has-multiple" data-product-ids="'.$metapress_products_list.'">';
                    $metapress_restricted_content .= '<p class="metapress-restricted-text">'.$wp_metapress_text_settings['restricted_text'].'</p>';

                    $metapress_restricted_content .= '<div class="metapress-login-notice"><p><small>*'.__('Accessing this content requires a', $wp_metapress_textdomain).' <a href="https://metamask.io/" target="_blank">MetaMask</a> '.__('account', $wp_metapress_textdomain).'</small>.</p><button class="metamask-connect-wallet">'.__('Connect Wallet', $wp_metapress_textdomain).'</button></div>';

                    $metapress_restricted_content .= '<div class="metapress-access-buttons">';
                    $metapress_restricted_content .= '<p class="metapress-checkout-purchase-text">'.$wp_metapress_text_settings['product_purchase_text'].':</p>';
                    foreach($metapress_required_products as $product_id) {
                        $product_name = get_the_title($product_id);
                        if( ! empty($metapress_checkout_page_url) ) {
                            $product_checkout_page_url = $metapress_checkout_page_url.'?mpp='.$product_id.'&mpred='.$current_page_url;
                            $metapress_restricted_content .= '<a href="'.$product_checkout_page_url.'" class="metapress-checkout-button">'.$product_name.'</a>';
                        }
                    }

                    $metapress_restricted_content .= '</div><div class="metapress-notice-box"></div></div>';
                } else {
                    $metapress_restricted_content .= $content;
                    if( ! empty($access_token) ) {
                    	$metapress_restricted_content .= '<div class="metapress-verification" data-address="'.$access_token->payment_owner.'" data-token="'.$access_token->token.'"></div>';
                    }
                }
            } else {
                $metapress_restricted_content .= $content;
            }
            return $metapress_restricted_content;
        }

		public function metapress_user_transactions_shortcode( $atts ) {
			global $wp_metapress_textdomain;
			$metapress_restricted_content = '<div class="metapress-login-notice metapress-align-content-center"><p><small>*'.__('Accessing this content requires a', $wp_metapress_textdomain).' <a href="https://metamask.io/" target="_blank">MetaMask</a> '.__('account', $wp_metapress_textdomain).'</small>.</p><button class="metamask-connect-wallet">'.__('Connect Wallet', $wp_metapress_textdomain).'</button></div>';

          $metapress_restricted_content .= '<div class="metapress-transaction metapress-border-box metapress-header"><div class="metapress-grid metapress-payment"><div class="metapress-setting-title">'.__('Product', $wp_metapress_textdomain).'</div><div class="metapress-setting-title">'.__('Paid With', $wp_metapress_textdomain).'</div><div class="metapress-setting-title">'.__('Amount', $wp_metapress_textdomain).'</div><div class="metapress-setting-title">'.__('Network', $wp_metapress_textdomain).'</div>';
          $metapress_restricted_content .= '<div class="metapress-setting-title">'.__('Date', $wp_metapress_textdomain).'</div><div class="metapress-setting-title">'.__('Status', $wp_metapress_textdomain).'</div><div class="metapress-setting-title">'.__('View', $wp_metapress_textdomain).'</div></div></div>';

    		  $metapress_restricted_content .= '<div id="metapress-user-transactions" class="border-box"></div>';

          $metapress_restricted_content .= '<div class="metapress-grid metapress-prev-next-buttons"><div class="metapress-nav-button prev"><div class="metapress-button">'.__('Previous', $wp_metapress_textdomain).'</div></div><div class="metapress-nav-button next"><div class="metapress-button">'.__('Next', $wp_metapress_textdomain).'</div></div></div>';
			return $metapress_restricted_content;
		}
}
$metaPress_plugin_content_filter_shortcodes = new MetaPress_Plugin_Content_Filter_Shortcodes();
