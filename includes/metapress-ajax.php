<?php
class MetaPress_Product_Ajax_Manager {

    public function __construct() {
        add_action( 'wp_ajax_verify_metapress_product_price_ajax_request', array($this, 'verify_metapress_product_price_ajax_request') );
        add_action( 'wp_ajax_nopriv_verify_metapress_product_price_ajax_request', array($this, 'verify_metapress_product_price_ajax_request') );

        add_action( 'wp_ajax_create_metapress_transaction_ajax_request', array($this, 'create_metapress_transaction_ajax_request') );
        add_action( 'wp_ajax_nopriv_create_metapress_transaction_ajax_request', array($this, 'create_metapress_transaction_ajax_request') );

        add_action( 'wp_ajax_update_metapress_transaction_ajax_request', array($this, 'update_metapress_transaction_ajax_request') );
        add_action( 'wp_ajax_nopriv_update_metapress_transaction_ajax_request', array($this, 'update_metapress_transaction_ajax_request') );

        add_action( 'wp_ajax_metapress_mark_transaction_as_paid_ajax_request', array($this, 'metapress_mark_transaction_as_paid_ajax_request') );
        add_action( 'wp_ajax_nopriv_metapress_mark_transaction_as_paid_ajax_request', array($this, 'metapress_mark_transaction_as_paid_ajax_request') );

        add_action( 'wp_ajax_metapress_remove_pending_transaction_ajax_request', array($this, 'metapress_remove_pending_transaction_ajax_request') );
        add_action( 'wp_ajax_nopriv_metapress_remove_pending_transaction_ajax_request', array($this, 'metapress_remove_pending_transaction_ajax_request') );

        add_action( 'wp_ajax_metapress_check_products_access_single_ajax_request', array($this, 'metapress_check_products_access_single_ajax_request') );
        add_action( 'wp_ajax_nopriv_metapress_check_products_access_single_ajax_request', array($this, 'metapress_check_products_access_single_ajax_request') );

        add_action( 'wp_ajax_metapress_create_nft_access_token_ajax_request', array($this, 'metapress_create_nft_access_token_ajax_request') );
        add_action( 'wp_ajax_nopriv_metapress_create_nft_access_token_ajax_request', array($this, 'metapress_create_nft_access_token_ajax_request') );

    }

    public function verify_metapress_product_price_ajax_request() {
        if( ! isset($_GET['product_id']) || empty($_GET['product_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Product ID'), 400);
        }

        if( ! isset($_GET['spender_address']) || empty($_GET['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }

        $product_access_data = array('has_access' => false);

        $product_id = intval($_GET['product_id']);
        $spender_address = sanitize_key($_GET['spender_address']);
        $product_price = get_post_meta($product_id, 'product_price', true);

        // CHECK IF SPENDER HAS ALREADY PAID
        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $existing_payment = $metapress_payments_manager->find_payment_for_product($product_id, $spender_address);

        // EXISTING PAID PAYMENT FOR PRODUCT EXISTS
        if( ! empty($existing_payment) ) {
            $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
            $token_exists = $metapress_access_token_manager->token_exists($product_id, $spender_address);
            if( ! empty($token_exists) ) {
                if( isset($token_exists->expires) && ( $token_exists->expires > current_time('timestamp') ) ) {
                    $product_access_data = array('has_access' => true, 'access_token' => $token_exists->token);
                } else {
                    $metapress_access_token_manager->delete_access_token($token_exists->id);
                }
            }

            // ADDRESS HAS PAID TRANSACTION SO CREATE NEW ACCESS TOKEN
            if( ! $product_access_data['has_access'] ) {
                $new_access_token = $metapress_access_token_manager->create_access_token(array('product_id' => $product_id, 'payment_owner' => $spender_address));
                if( ! empty($new_access_token) ) {
                    $product_access_data = array('has_access' => true, 'access_token' => $new_access_token);
                }
            }

        } else {
            //EXISTING PENDING PAYMENT FOR PRODUCT
            $existing_payment = $metapress_payments_manager->find_pending_payment_for_product($product_id, $spender_address);
            if( ! empty($existing_payment) ) {
                $product_access_data = (array) $existing_payment;
            }
        }

        if( ! empty($product_price) ) {
            $product_access_data['price'] = $product_price;
        }
        echo json_encode($product_access_data);
        wp_die();
    }

    public function metapress_check_products_access_single_ajax_request() {
        if( ! isset($_GET['products']) || empty($_GET['products']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Products'), 400);
        }

        if( ! isset($_GET['spender_address']) || empty($_GET['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }
        $product_ids = array();
        $product_access_data = array('has_access' => false);

        if( isset($_GET['products']) && ! empty($_GET['products']) ) {
            $product_ids = $_GET['products'];

            $spender_address = sanitize_key($_GET['spender_address']);

            // CHECK IF SPENDER HAS ALREADY PAID
            $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();

            foreach($product_ids as $product_id) {
                $product_id = sanitize_key($product_id);

                $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
                $token_exists = $metapress_access_token_manager->token_exists($product_id, $spender_address);
                if( ! empty($token_exists) ) {
                    if( isset($token_exists->expires) && ($token_exists->expires > current_time('timestamp') ) ) {
                        $product_access_data = array('has_access' => true, 'access_token' => $token_exists->token, 'product_id' => $product_id);
                        break;
                    } else {
                        $metapress_access_token_manager->delete_access_token($token_exists->id);
                    }
                }

                if( ! $product_access_data['has_access'] ) {
                    $existing_payment = $metapress_payments_manager->find_payment_for_product($product_id, $spender_address);
                    // EXISTING PAID PAYMENT FOR PRODUCT EXISTS
                    if( ! empty($existing_payment) ) {
                        // ADDRESS HAS PAID TRANSACTION SO CREATE NEW ACCESS TOKEN
                        $new_access_token = $metapress_access_token_manager->create_access_token(array('product_id' => $product_id, 'payment_owner' => $spender_address));
                        if( ! empty($new_access_token) ) {
                            $product_access_data = array('has_access' => true, 'access_token' => $new_access_token, 'product_id' => $product_id);
                            break;
                        }
                    } else {
                        //EXISTING PENDING PAYMENT FOR PRODUCT
                        $existing_payment = $metapress_payments_manager->find_pending_payment_for_product($product_id, $spender_address);
                        if( ! empty($existing_payment) ) {
                            $product_access_data = (array) $existing_payment;
                            break;
                        }
                    }
                }
            }
        } else {
            metapress_wp_plugin_exit_ajax(__('Missing product IDs'), 400);
        }

        echo json_encode($product_access_data);
        wp_die();
    }

    public function create_metapress_transaction_ajax_request() {
        if( ! isset($_POST['product_id']) || empty($_POST['product_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Product ID'), 400);
        }

        if( ! isset($_POST['transaction_hash']) || empty($_POST['transaction_hash']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing transaction'), 400);
        }

        if( ! isset($_POST['spender_address']) || empty($_POST['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }

        if( ! isset($_POST['token']) || empty($_POST['token']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing token that was used to pay'), 400);
        }

        if( ! isset($_POST['token_amount']) || empty($_POST['token_amount']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing payment amount'), 400);
        }

        if( ! isset($_POST['network']) || empty($_POST['network']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing network provider'), 400);
        }

        if( isset($_POST['txn_status']) && $_POST['txn_status'] == 'approval' ) {
            $transaction_status = 'approval';
        } else {
            $transaction_status = 'pending';
        }

        if( isset($_POST['contract_address']) && ! empty($_POST['contract_address']) ) {
            $contract_address = sanitize_key($_POST['contract_address']);
        } else {
            $contract_address = null;
        }

        $product_id = sanitize_text_field($_POST['product_id']);
        $token_used = sanitize_text_field($_POST['token']);
        $sent_amount = sanitize_text_field($_POST['token_amount']);
        $token_network = sanitize_text_field($_POST['network']);
        $transaction_hash = sanitize_text_field($_POST['transaction_hash']);
        $spender_address = strtolower($_POST['spender_address']);
        $spender_address = sanitize_key($_POST['spender_address']);

        $transaction_fee = bcmul($sent_amount, 0.01, 18);
        $token_amount = bcsub($sent_amount, $transaction_fee, 18);

        $new_payment = array(
            'product_id' => $product_id,
            'payment_owner' => strtolower($spender_address),
            'token' => $token_used,
            'token_amount' => $token_amount,
            'sent_amount' => $sent_amount,
            'network' => $token_network,
            'transaction_time' => current_time('timestamp'),
            'transaction_hash' => $transaction_hash,
            'transaction_status' => $transaction_status,
            'contract_address' => $contract_address,
        );

        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $new_payment_id = $metapress_payments_manager->create_payment($new_payment);
        if( ! empty($new_payment_id) ) {
            echo json_encode(array('success' => true));
        } else {
            metapress_wp_plugin_exit_ajax(__('Could create a new API Key'), 400);
        }
        wp_die();
    }

    public function update_metapress_transaction_ajax_request() {
        if( ! isset($_POST['product_id']) || empty($_POST['product_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Product ID'), 400);
        }

        if( ! isset($_POST['transaction_id']) || empty($_POST['transaction_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing transaction ID'), 400);
        }

        if( ! isset($_POST['transaction_hash']) || empty($_POST['transaction_hash']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing transaction'), 400);
        }

        if( ! isset($_POST['spender_address']) || empty($_POST['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }

        $product_id = sanitize_text_field($_POST['product_id']);
        $transaction_id = sanitize_text_field($_POST['transaction_id']);
        $transaction_hash = sanitize_text_field($_POST['transaction_hash']);
        $spender_address = strtolower($_POST['spender_address']);
        $spender_address = sanitize_key($spender_address);

        $update_payment = array(
            'transaction_time' => current_time('timestamp'),
            'transaction_hash' => $transaction_hash,
            'transaction_status' => 'pending',
        );

        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $existing_payment = $metapress_payments_manager->get_payment($transaction_id);
        if( ! empty($existing_payment) && $existing_payment->payment_owner == $spender_address) {
            $payment_updated = $metapress_payments_manager->update_payment($transaction_id, $update_payment);
        } else {
            $payment_updated = false;
        }

        if( ! empty($payment_updated) ) {
            echo json_encode(array('updated' => true));
        } else {
            metapress_wp_plugin_exit_ajax(__('Could not update approval transaction'), 400);
        }
        wp_die();
    }

    public function metapress_remove_pending_transaction_ajax_request() {
        if( ! isset($_POST['product_id']) || empty($_POST['product_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Product ID'), 400);
        }

        if( ! isset($_POST['transaction_hash']) || empty($_POST['transaction_hash']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing transaction'), 400);
        }

        if( ! isset($_POST['spender_address']) || empty($_POST['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }

        $payment_deleted = false;

        $product_id = sanitize_text_field($_POST['product_id']);
        $transaction_hash = sanitize_text_field($_POST['transaction_hash']);
        $spender_address = strtolower($_POST['spender_address']);
        $spender_address = sanitize_key($spender_address);

        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $existing_payment = $metapress_payments_manager->find_payment_with_hash($product_id, $spender_address, $transaction_hash);
        if( ! empty($existing_payment) && $existing_payment->transaction_status == 'pending' ) {
            $payment_deleted = $metapress_payments_manager->delete_payment($existing_payment->id);
        }
        if( $payment_deleted ) {
            echo json_encode(array('success' => true));
        } else {
            metapress_wp_plugin_exit_ajax(__('Transaction was not deleted.'), 400);
        }
        wp_die();
    }

    public function metapress_mark_transaction_as_paid_ajax_request() {
        if( ! isset($_POST['product_id']) || empty($_POST['product_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Product ID'), 400);
        }

        if( ! isset($_POST['transaction_hash']) || empty($_POST['transaction_hash']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing transaction'), 400);
        }

        if( ! isset($_POST['spender_address']) || empty($_POST['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }

        $payment_updated = false;

        $product_id = sanitize_text_field($_POST['product_id']);
        $transaction_hash = sanitize_text_field($_POST['transaction_hash']);
        $spender_address = strtolower($_POST['spender_address']);
        $spender_address = sanitize_key($spender_address);

        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $existing_payment = $metapress_payments_manager->find_payment_with_hash($product_id, $spender_address, $transaction_hash);
        if( ! empty($existing_payment) ) {
            if( $existing_payment->transaction_status == 'paid' ) {
              $payment_updated = true;
            } else {
              $payment_updated = $metapress_payments_manager->update_payment($existing_payment->id, array('transaction_status' => 'paid'));
            }

        }
        if( $payment_updated ) {
            $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();
            $new_access_token = $metapress_access_token_manager->create_access_token(array('product_id' => $product_id, 'payment_owner' => $spender_address));
            echo json_encode(array('success' => true, 'access_token' => $new_access_token));
        } else {
            metapress_wp_plugin_exit_ajax(__('Transaction failed to be marked as paid'), 400);
        }
        wp_die();
    }

    public function metapress_create_nft_access_token_ajax_request() {
        if( ! isset($_POST['product_id']) || empty($_POST['product_id']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing Product ID'), 400);
        }

        if( ! isset($_POST['nft_owner_verified']) || empty($_POST['nft_owner_verified']) ) {
            metapress_wp_plugin_exit_ajax(__('NFT verification missing'), 400);
        }

        if( ! isset($_POST['spender_address']) || empty($_POST['spender_address']) ) {
            metapress_wp_plugin_exit_ajax(__('Missing spender address'), 400);
        }

        if( isset($_POST['mpredirect']) && ! empty($_POST['mpredirect']) ) {
            $mp_redirect_url = $_POST['mpredirect'];
        } else {
            $mp_redirect_url = null;
        }

        $nft_nonce_check = check_ajax_referer( 'metapress-nft-verified-nonce', 'nft_owner_verified' );

        if( ! empty($nft_nonce_check) ) {
            $product_id = sanitize_text_field($_POST['product_id']);
            $spender_address = strtolower($_POST['spender_address']);
            $spender_address = sanitize_key($spender_address);
            $product_access_data = array('success' => false);

            $metapress_access_token_manager = new METAPRESS_ACCESS_TOKENS_MANAGER();

            $token_exists = $metapress_access_token_manager->token_exists($product_id, $spender_address);
            if( ! empty($token_exists) ) {
                if( isset($token_exists->expires) && ( $token_exists->expires > current_time('timestamp') ) ) {
                    $product_access_data = array('success' => true, 'access_token' => $token_exists->token);
                    $new_access_token = $token_exists->token;
                } else {
                    $metapress_access_token_manager->delete_access_token($token_exists->id);
                }
            }

            if( ! $product_access_data['success'] ) {
                $new_access_token = $metapress_access_token_manager->create_access_token(array('product_id' => $product_id, 'payment_owner' => $spender_address));
                if( ! empty($new_access_token) ) {
                    $product_access_data = array('success' => true, 'access_token' => $new_access_token);
                }
            }

            if( ! empty($mp_redirect_url) ) {
                $product_access_data['redirect'] = $mp_redirect_url.'?mpatok='.$new_access_token;
            }
            echo json_encode($product_access_data);
        } else {
            metapress_wp_plugin_exit_ajax(__('Invalid nonce request'), 400);
        }

        wp_die();
    }
}
$metapress_product_ajax_manager = new MetaPress_Product_Ajax_Manager();


function metapress_wp_plugin_exit_ajax($error_message, $error_code) {
    if( empty($error_code) ) {
        $error_code = 400;
    }
    status_header($error_code);
    echo $error_message;
    exit;
}
