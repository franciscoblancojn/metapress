<?php

class MetaPress_Plugin_Rest_Api_Manager extends WP_REST_Controller {
    public $current_time;
    public function __construct() {
        $this->current_time = current_time('timestamp', 1);
        $this->request_key_match = get_option('metapress_api_request_match');
        add_action( 'rest_api_init', array($this, 'register_metapress_plugin_api_endpoint_routes') );
    }

    public function register_metapress_plugin_api_endpoint_routes() {

        register_rest_route( 'metapress/v2', '/transactions/', array(
                'methods'  => 'GET',
                'callback' => array($this, 'list_user_transactions'),
                'permission_callback' => array($this, 'metapress_permissions_check' ),
            )
        );

        register_rest_route( 'metapress/v2', '/productdata/', array(
                'methods'  => 'GET',
                'callback' => array($this, 'list_product_content'),
                'permission_callback' => array($this, 'metapress_permissions_check' ),
            )
        );
    }

    private function verify_api_key($api_key) {
        // $license_is_valid = false;
        // $metapress_license_key_manager = new MetaPress_License_Key_Manager();
        // $license = $metapress_license_key_manager->get_license_by_key($api_key);
        // if( ! empty($license) ) {
        //     if( $license->expires > $this->current_time ) {
        //         $license_is_valid = true;
        //     }
        // }
        // return $license_is_valid;
        return true;
    }

    public function metapress_permissions_check( $request ) {
        $api_key = $request->get_param('api_key');
        if( $this->verify_api_key($api_key) ) {
          return true;
        } else {
          return false;
        }
    }

    public function list_user_transactions($request) {
        $mp_request_key = $request->get_param('request_key');
        //$mp_api_key = $request->get_param('api_key');
        $wallet_address = $request->get_param('mpwalletaddress');
        if( empty($wallet_address) || empty($mp_request_key) ) {
            return $this->handle_error( __('Invalid request', 'rogue-themes') );
        }
        if( $mp_request_key != $this->request_key_match ) {
            return $this->handle_error( __('Invalid request', 'rogue-themes') );
        }

        $offset = 0;
        $token = null;
        $payment_status = null;
        $filters = array();
        if( isset($_GET["offset"]) ) {
            $offset = intval($_GET["offset"]);
        }

        if( isset($_GET["token"]) && ! empty($_GET["token"]) ) {
            $filters['token'] = sanitize_key($_GET["token"]);
        }

        if( isset($_GET["status"]) && ! empty($_GET["status"]) ) {
            $filters['status'] = sanitize_text_field($_GET["status"]);
        }

        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $payments = $metapress_payments_manager->get_wallet_address_payments($wallet_address, $offset, $filters);
        $count_payments = 0;
        $found_payments = array();
        if( ! empty($payments) ) {
            $count_payments = count($payments);
            foreach($payments as $payment) {
                $view_transaction_url = $metapress_payments_manager->find_network_explorer_url($payment->network);
                if (substr($view_transaction_url, -1) !== '/') {
                    $view_transaction_url .= '/';
                }
                $view_transaction_url .= 'tx/'.$payment->transaction_hash;
                $product_name = get_the_title($payment->product_id);
                $add_payment = array(
                    'id' => $payment->id,
                    'token'  => $payment->token,
                    'sent_amount'  => number_format($payment->sent_amount, 8),
                    'transaction_time' => wp_date('F j, Y', $payment->transaction_time),
                    'network' => $payment->network,
                    'transaction_hash' => $payment->transaction_hash,
                    'transaction_status' => $payment->transaction_status,
                    'view_url' => $view_transaction_url,
                    'product_name' => $product_name,
                    'product_id' => $payment->product_id,
                );
                $found_payments[] = $add_payment;
            }
        }
        return new WP_REST_Response(array('count' => $count_payments, 'payments' => $found_payments), 200);
    }

    public function list_product_content($request) {
        $mp_request_key = $request->get_param('request_key');
        $wallet_address = $request->get_param('mpwalletaddress');
        $product_id = $request->get_param('productid');

        if( empty($wallet_address) || empty($mp_request_key) ) {
            return $this->handle_error( __('Invalid request', 'rogue-themes') );
        }
        if( $mp_request_key != $this->request_key_match ) {
            return $this->handle_error( __('Invalid request', 'rogue-themes') );
        }
        $has_access = false;
        $product_content = array();

        $metapress_payments_manager = new METAPRESS_PAYMENTS_MANAGER();
        $existing_payment = $metapress_payments_manager->find_payment_for_product($product_id, $wallet_address);

        if( ! empty($existing_payment) ) {
            $has_access = true;
            $existing_payment->product_id;
            $product_content = $this->get_pages_with_product($existing_payment->product_id);
        }
        return new WP_REST_Response(array('has_access' => $has_access, 'content' => $product_content), 200);
    }

    private function get_pages_with_product($product_id) {
      $found_pages = array();
      $metapress_supported_post_types = get_option('metapress_supported_post_types');
      if( empty($metapress_supported_post_types) ) {
        $metapress_supported_post_types = array('post', 'page');
      }
      $page_args = array(
        'post_type' => $metapress_supported_post_types,
        'post_status' => 'publish',
        'numberposts' => -1,
        'fields' => 'ids',
        'meta_query' => array(
          array(
            'key' => 'metapress_required_products',
            'value' => $product_id,
            'compare' =>  'LIKE'
          ),
        ),
      );
      $pages_with_restrictions = get_posts($page_args);
      if( ! empty($pages_with_restrictions) ) {
        foreach($pages_with_restrictions as $restricted_page_id) {
          $found_pages[] =  array(
            'id' => $restricted_page_id,
            'name' => get_the_title($restricted_page_id),
            'link' => get_the_permalink($restricted_page_id),
            'type' => 'page',
          );
        }
      }
      return $found_pages;
    }

    public function handle_error( $error_message ) {
        return new WP_REST_Response(array(
            'error' => $error_message,
        ), 400);
    }

}
$metaPress_plugin_rest_api_manager = new MetaPress_Plugin_Rest_Api_Manager();
