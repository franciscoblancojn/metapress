<?php

function metapress_plugin_update_version_check() {
    global $wp_metapress_version;
    if(INSTALLED_METAPRESS_PLUGIN_VERSION !== $wp_metapress_version) {
        run_metapress_plugin_automatic_updates();
    }
}
add_action( 'plugins_loaded', 'metapress_plugin_update_version_check' );

function run_metapress_plugin_automatic_updates() {
    global $wp_metapress_version;
    $needs_update = false;
    if(!empty($wp_metapress_version)) {
        $current_version_number = intval(str_replace(".","",$wp_metapress_version));
    } else {
        $current_version_number = 0;
    }
    if($current_version_number < 102) {
        require_once(METAPRESS_PLUGIN_BASE_DIR.'/includes/create-tables.php');
    }

    if($current_version_number < 113) {
        require_once(METAPRESS_PLUGIN_BASE_DIR.'/updates/update-networks.php');
    }

    if($current_version_number < 117) {
        $set_metpress_contact_address = (object) array(
        	'ropsten' => '0x9A434d8c8B9C99ACa036130055511Dc5152a5ae2',
        	'mainnet' => '0x00c5A679d3Ae7261e021FF80DF210De513b38042',
        	'matictestnet' => '0xeDd4e25A3b3eD2e0cc5B32a567b8EdfcbfBcCFcA',
        	'maticmainnet' => '0x2486bD244919E55d8b98053c46e4eB6c10ACf9E7',
        	'binancetestnet' => '0x9bC293b22c74a8b2eEd677e77A7c90c7aE34ace4',
        	'binancesmartchain' => '0x9bC293b22c74a8b2eEd677e77A7c90c7aE34ace4'
        );
        update_option('metapress_contract_addresses', $set_metpress_contact_address);
    }

    update_option('wp_metapress_plugin_version', '1.2.1');
}
