<?php

function metapress_plugin_request_update_filter($queryArgs) {
    $metapress_contract_addresses = get_option('metapress_contract_addresses');
    if( ! empty($metapress_contract_addresses) && isset($metapress_contract_addresses->mainnet) && $metapress_contract_addresses->mainnet == '0x00c5A679d3Ae7261e021FF80DF210De513b38042' ) {
        $queryArgs['can_update'] = true;
        $queryArgs['update_key'] = get_option('metapress_update_request_key');
    }
    return $queryArgs;
}
