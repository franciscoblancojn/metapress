<?php


$metapress_nft_contract_list = get_option('metapress_nft_contract_list');
$metapress_custom_tokens_list = get_option('metapress_custom_tokens_list');

if( ! empty($metapress_nft_contract_list) ) {
    foreach($metapress_nft_contract_list as $key => &$nft_contract) {
        if( $nft_contract['network'] == 'mainnet' ) {
            $nft_contract['networkname'] = 'Ethereum Mainnet';
            $nft_contract['chainid'] = '0x1';
        }
        if( $nft_contract['network'] == 'maticmainnet' ) {
            $nft_contract['networkname'] = 'Polygon (MATIC)';
            $nft_contract['chainid'] = '0x89';
        }
    }
    update_option('metapress_nft_contract_list', $metapress_nft_contract_list);
}

if( ! empty($metapress_custom_tokens_list) ) {
    foreach($metapress_custom_tokens_list as $key => &$custom_token) {
        if( $custom_token['network'] == 'mainnet' ) {
            $custom_token['networkname'] = 'Ethereum Mainnet';
            $custom_token['chainid'] = '0x1';
            $custom_token['explorer'] = 'https://etherscan.io/';
        }
        if( $custom_token['network'] == 'maticmainnet' ) {
            $custom_token['networkname'] = 'Polygon (MATIC)';
            $custom_token['chainid'] = '0x89';
            $custom_token['explorer'] = 'https://polygonscan.com/';
        }
        if( $custom_token['test_network'] == 'ropsten' ) {
            $custom_token['test_networkname'] = 'Ethereum Ropsten';
            $custom_token['test_chainid'] = '0x3';
            $custom_token['test_explorer'] = 'https://ropsten.etherscan.io/';
        }
        if( $custom_token['test_network'] == 'matictestnet' ) {
            $custom_token['test_networkname'] = 'Polygon (mumbai)';
            $custom_token['test_chainid'] = '0x80001';
            $custom_token['test_explorer'] = 'https://mumbai.polygonscan.com/';
        }
    }
    update_option('metapress_custom_tokens_list', $metapress_custom_tokens_list);
}

$metaproduct_post_args = array(
    'post_type' => 'metapress_product',
    'numberposts' => -1,
    'meta_key' => 'product_nft_access_list',
    'meta_compare' => 'EXISTS',
    'fields' => 'ids',
);

$metapress_product_list = get_posts($metaproduct_post_args);

if( ! empty($metapress_product_list) ) {
    foreach($metapress_product_list as $metaproduct_id) {
        $product_nft_access_list = get_post_meta($metaproduct_id, 'product_nft_access_list', true );
        if( ! empty($product_nft_access_list) ) {
            foreach($product_nft_access_list as $key => &$nft_access) {
                if( $nft_access['network'] == 'mainnet' ) {
                    $nft_access['networkname'] = 'Ethereum Mainnet';
                    $nft_access['chainid'] = '0x1';
                }
                if( $nft_access['network'] == 'maticmainnet' ) {
                    $nft_access['networkname'] = 'Polygon (MATIC)';
                    $nft_access['chainid'] = '0x89';
                }
            }
            update_post_meta($metaproduct_id, 'product_nft_access_list', $product_nft_access_list);
        }
    }
}
